#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <GL/freeglut.h>
#include "AppController.h"
#include "Scribble.h"
#include "Button.h"

class Controller : public AppController {
    Scribble left;
    Scribble right;
    Button duplicate;
    Button up;
    Button down;

public:
    Controller(){
        duplicate = Button("Duplicate", 0.3f, -0.7f);
        up = Button("UP", -0.8f, -0.7f);
        down = Button("DOWN", -0.4f, -0.7f);
    }

    void leftMouseDown(float x, float y) {
        if (x < 0.0f && y > -0.6) {
            left.addPoint(x, y, Color(1.0f, 0.0f, 0.0f));
        }

        if (duplicate.contains(x, y)) {
            std::cout << "Duplicate" << std::endl;
            right = left;
            right.setX(right.getX() + 1.0f);
        } else if (up.contains(x, y)) {
            std::cout << "Move up" << std::endl;
            left.moveUp();
        } else if (down.contains(x, y)) {
            std::cout << "Move down" << std::endl;
            left.moveDown();
        }
    }

    void mouseMotion(float x, float y) {
        if (x < 0.0f && y > -0.6) {
            left.addPoint(x, y, Color(1.0f, 0.0f, 0.0f));
        }
    }

    void render() {
        glColor3f(0.0f, 0.0f, 0.0f);
        glLineWidth(1.0f);

        glBegin(GL_LINES);
            glVertex2f(0.0f, 1.0f);
            glVertex2f(0.0f, -1.0f);
        glEnd();

        left.draw();
        right.draw();
        up.draw();
        down.draw();
        duplicate.draw();
    }
};

#endif